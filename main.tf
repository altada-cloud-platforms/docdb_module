resource "aws_docdb_subnet_group" "service" {
  name       = "tf-${var.name}"
  subnet_ids = var.private_subnets
}

resource "aws_docdb_cluster_instance" "service" {
  count              = var.instances
  identifier         = "${var.name}-instance-${count.index}"
  cluster_identifier = "${aws_docdb_cluster.service.id}"
  instance_class     = "${var.instance_class}"
}

resource "aws_docdb_cluster" "service" {
  skip_final_snapshot     = true
  db_subnet_group_name    = "${aws_docdb_subnet_group.service.name}"
  cluster_identifier      = "${var.name}-cluster"
  engine                  = "docdb"
  master_username         = "${var.docdb_username}"
  master_password         = "${var.docdb_password}"
  db_cluster_parameter_group_name = "${aws_docdb_cluster_parameter_group.service.name}"
  vpc_security_group_ids = ["${var.security_group_id}"]
}

resource "aws_docdb_cluster_parameter_group" "service" {
  family = "docdb4.0"
  name = "${var.name}-parameter-group"

  parameter {
    name  = "tls"
    value = "disabled"
  }
}
