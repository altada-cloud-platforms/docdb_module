variable "name" {
  type        = string
  default     = ""
  description = "Document DB name"
}

variable "private_subnets" {
  type        = list(string)
  description = "List of VPC subnet IDs to place DocumentDB instances in"
}

# https://docs.aws.amazon.com/documentdb/latest/developerguide/limits.html#suported-instance-types
variable "instance_class" {
  type        = string
  default     = "db.r4.large"
  description = "The instance class to use. For more details, see https://docs.aws.amazon.com/documentdb/latest/developerguide/db-insta nce-classes.html#db-instance-class-specs"
}

variable "instances" {
  type        = number
  default     = 1
  description = "DocumentDB instances"
}

variable "docdb_password" {
  type        = string
  default     = ""
  description = "DocumentDB password"
}

variable "docdb_username" {
  type        = string
  default     = ""
  description = "DocumentDB username"
}

variable "security_group_id" {
  type        = string
  default     = ""
  description = "ID of the DocumentDB cluster Security Group"
}
